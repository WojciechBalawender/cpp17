/*Test RVO - Return Value optimatization
  Wylaczenie optymalizacji: -fno-elide-constructors
*/

#define I_STOP 100000

class BigClass
{
    double big[50000];
};

BigClass rvoTest ()
{
    BigClass A;
    return A;
}

int main()
{
	for(int i = 0; i < I_STOP; i++)
		BigClass B = rvoTest();
}
