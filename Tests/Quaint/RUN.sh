#!/bin/bash

#set +v
current=$(pwd)


#PREAPRE

###Nazwy plikow zaszyte w kodzie###

#Przygotowanie struktury katalogow:
find . -type d -print0 > ../Logs/struct.tmp
cd ../Bin
xargs -0 mkdir -p < ../Logs/struct.tmp
rm ../Logs/struct.tmp
cd $current

compilers=("${3}")

echo
echo -e "\e[95mKompilacja z QUAINT (specyficzne pliki)...\e[39m"
for compiler in ${compilers[@]} 
do
	echo -e "\n--- KOMPILACJA z $current\t$(date)" >> ../Logs/results_${compiler}.csv 
	echo "1. Z obsluga RVO"
	if [ $compiler == "cl" ] ; then
		   echo Nieobslugiwane dla tego kompilatora
		   echo -e "Nieobslugiwane dla tego kompilatora\t t=????" >> ../Logs/results_${compiler}.csv
		   continue
	fi
	../stopwatch.${1} "${compiler} -S -std=c++17 -fverbose-asm -o ../Bin/RVO/WithRVO.${compiler}.s RVO/RVO.cpp >> ../Logs/errors.txt" >> ../Logs/results_${compiler}.csv
	../stopwatch.${1} "${compiler} -o ../Bin/RVO/WithRVO.${compiler}.${1} ../Bin/RVO/WithRVO.${compiler}.s >> ../Logs/errors.txt" >> ../Logs/results_${compiler}.csv
	echo "2. Bez obslugi RVO..."
	../stopwatch.${1} "${compiler} -S -std=c++17 -fverbose-asm -fno-elide-constructors -o ../Bin/RVO/WithoutRVO.${compiler}.s RVO/RVO.cpp >> ../Logs/errors.txt" >> ../Logs/results_${compiler}.csv
	../stopwatch.${1} "${compiler} -o ../Bin/RVO/WithoutRVO.${compiler}.${1} ../Bin/RVO/WithoutRVO.${compiler}.s >> ../Logs/errors.txt" >> ../Logs/results_${compiler}.csv

	#Uruchomienie
	echo -e "\n--- URUCHOMIENIE z $current:\t$(date)" >> ../Logs/results_${compiler}.csv
	../stopwatch.${1} "${2} ../Bin/RVO/WithRVO.${compiler}.${1}" >> ../Logs/results_${compiler}.csv
	../stopwatch.${1} "${2} ../Bin/RVO/WithoutRVO.${compiler}.${1}" >> ../Logs/results_${compiler}.csv

done
	find . -name '*.obj' -delete