@ECHO OFF
ECHO Inicjalizacja...

reg query HKEY_CLASSES_ROOT\Directory\Background\shell\openCygwin>nul
if %ERRORLEVEL% EQU 0 goto INIT
regedit /s %cd%/Utilities/WindowsShAssociate.reg

:INIT
REM Prosze ustawic ponizej zmienne srodowiskowe:
set PATH=G:\Compilers\MinGW_7.3\bin;G:\Compilers\MinGW_8.2\bin;G:\Compilers\LLVM\bin;G:\Compilers\MinGW_4.4.1\bin;G:\Compilers\w32MinGW_8.2\bin;%PATH% 
call "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvars64.bat"
REM Naprawia pliki wyedytowane "zlym" edytorem
REM start Utilities/RepairCRLF.sh
taskkill /f /im explorer.exe
RUNNER.sh