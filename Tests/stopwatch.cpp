/* Program odpowiedzialny za obliczanie czasu wykonania programu
  (zadanego jako argument wywolania)
*/

#include "Utilities/TestUtilities.hpp"
#define REPEAT 3 //Im wiecej powtorzen - tym dokladniejszy rezultat
#include <iostream>
using namespace std;
int main(int argc, char *argv[])
{
   if(argc < 2) cout << "Aby rozpoczac testy, uruchom RUNNER\n";
   else
   {
       cout.precision(3);
       cout.setf(ios::fixed);
       Stopwatch time;
	   int error;
       for(int i = 0; i < REPEAT; i++)
       {
           error = system(argv[1]);
       }
       if(!error) cout << argv[1] << "\tt = " << time.result() / REPEAT << '\n';
       else cout << argv[1]  << " \t[EXECUTION ERROR]\n";
   }
}
