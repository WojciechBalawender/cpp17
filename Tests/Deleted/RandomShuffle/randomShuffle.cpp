
#include <ctime>
#include <algorithm>
#include <vector>
#include <cstdlib>

int myrandom (int i) { return std::rand()%i;}

int main () {
  std::srand (unsigned(std::time(NULL)));
  std::vector<int> v;

  for (int i=1; i< 20; ++i) v.push_back(i);

  std::random_shuffle ( v.begin(), v.end() );
  std::random_shuffle ( v.begin(), v.end(), myrandom);

  return 0;
}
