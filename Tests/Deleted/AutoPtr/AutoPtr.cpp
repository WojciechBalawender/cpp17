#include <iostream>
#include <memory>

int main () {

  std::auto_ptr<int> t1 (new int);
  *t1.get()=10;
  std::auto_ptr<int> t2 (t1);
  std::cout << "t2 " << *t2 << '\n';
}
