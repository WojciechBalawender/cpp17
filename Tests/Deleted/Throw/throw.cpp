#include <exception>

void one() throw() {}
void two() throw(std::exception) {}

int main()
{
}
