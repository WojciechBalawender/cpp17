#include <functional>
#include <vector>
#include <algorithm>

using namespace std;

int main () {

  vector <string*> liczby;

  liczby.push_back ( new string ("jeden") );
  liczby.push_back ( new string ("dwa") );

  vector <int> lengths ( liczby.size() );

  transform (liczby.begin(), liczby.end(), lengths.begin(), mem_fun(&string::length));
}
