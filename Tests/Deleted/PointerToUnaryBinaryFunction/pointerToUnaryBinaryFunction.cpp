#include <functional>
#include <algorithm>
#include <cmath>

bool test(char c)
{
    return std::string("abcde").find(c) != std::string::npos;
}

int main ()
{
  std::pointer_to_unary_function <double,double> Log (log);
  float numbers[] = {0.5f, 1.0f, 1.5f, 3.0f};
  float logs[5];
  transform (numbers, numbers+5, logs, Log);
  std::ptr_fun(test);
  return 0;

}
