#define I_STOP 10000000
#include <iostream>
using namespace std;

template<typename T>
T funkcja_szablonowa(T t)
{
    if constexpr (is_same_v<T, int>) return t/2;
    else if constexpr (is_same_v<T, string>) return t.substr(2);
    else return t;
}

int main()
{
    for(int i = 0; i < I_STOP; i++)
    {
        int z = funkcja_szablonowa(120);
        string b = funkcja_szablonowa("ABC");
    }
}
