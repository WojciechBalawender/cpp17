#define I_STOP 10000000
#include <iostream>
using namespace std;

template<typename T>
T funkcja_szablonowa(T t)
{
    if (is_same_v<T, int>) return t/2;
    else if(is_same_v<T, string>) return t.substr(2);
    else return t;
}

int main()
{
    for(int i = 0; i < I_STOP; i++)
    {
        int z = funkcja_szablonowa(120);
        string a = funkcja_szablonowa("ABC");
    }
}
