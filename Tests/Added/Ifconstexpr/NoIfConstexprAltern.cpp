#define I_STOP 10000000
#include <iostream>
using namespace std;

/*Specjalizowana funkcja */
string funkcja_szablonowa(string t)
{
    return t.substr(2);
}

/*Specjalizowana funkcja 2 */
const char * funkcja_szablonowa(const char * t)
{
    return t;
}

template<typename T>
T funkcja_szablonowa(T t)
{
    return t/2;
}

int main()
{
    for(int i = 0; i < I_STOP; i++)
    {
        int z = funkcja_szablonowa(120);
        string a = funkcja_szablonowa("ABC");
    }
}
