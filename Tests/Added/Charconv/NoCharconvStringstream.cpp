#define I_STOP 400000 //(Wieksza probka = wieksza dokladnosc testu)

#include <sstream>
#include <iostream>
#include <cstdlib>
#include <ctime>

double simpleDouble(double dmin, double dmax)
{
    double f = (double)rand() / RAND_MAX;
    return dmin + f * (dmax - dmin);
}

using namespace std;
int main()
{
    srand(time(nullptr));
    double po_konwersji;
    stringstream strum;
    cout.precision(15);
    strum.precision(15); //zadac pytanie, czemu strum.precision(9) dziala, a 10 nie
    strum.setf(ios::fixed);

    for(int i = 0; i < I_STOP; i++)
    {
       double PROTOTYPE = simpleDouble(-7654321,7654321);
       strum << PROTOTYPE;
       strum >> po_konwersji;
       strum.clear(strum.rdstate() & ~ios::eofbit);
       if(po_konwersji!=PROTOTYPE)
       {
           cerr << "Blad NoCharconv(stringstream)\n";
           return -1;
       }
    }
}
