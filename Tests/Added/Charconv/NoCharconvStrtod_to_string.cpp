#define I_STOP 400000 //(Wieksza probka = wieksza dokladnosc testu)

#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>

double simpleDouble(double dmin, double dmax)
{
    double f = (double)rand() / RAND_MAX;
    return dmin + f * (dmax - dmin);
}

using namespace std;
int main()
{
    srand(time(nullptr));
    double po_konwersji;
    string strum;
    cout.precision(15);

    for(int i = 0; i < I_STOP; i++)
    {
       double PROTOTYPE = simpleDouble(-7654321,7654321);
       strum = to_string(PROTOTYPE);
       po_konwersji = strtod (strum.c_str(), nullptr);
       if(abs(po_konwersji-PROTOTYPE) >= 0.000001)
       {
           cout << po_konwersji << endl;
           cout << PROTOTYPE << endl;
           cerr << "Blad NoCharconv(stringstream)\n";
           return -1;
       }
    }
}
