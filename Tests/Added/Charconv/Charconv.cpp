#define I_STOP 400000 //(Wieksza probka = wieksza dokladnosc testu)

#include <iostream>
#include <charconv>
#include <ctime>

double simpleDouble(double dmin, double dmax)
{
    double f = (double)rand() / RAND_MAX;
    return dmin + f * (dmax - dmin);
}

using namespace std;
int main()
{
    srand(time(nullptr));

  //  double po_konwersji;
    int po_konwersji;
    cout.precision(15);

    for(int i = 0; i < I_STOP; i++)
    {
       char temp[26] = {0};
     //  double PROTOTYPE = simpleDouble(-7654321,7654321);
       int PROTOTYPE = int(simpleDouble(-7654321,7654321)); //!!!!!!!!!!!!!!!!!!!!
     //  to_chars(temp, temp+24, PROTOTYPE, chars_format::fixed, 24); //Nie dziala pod gcc
       to_chars(temp, temp+24, PROTOTYPE);
       from_chars(temp, temp +24,po_konwersji);
       if(po_konwersji!=PROTOTYPE)
       {
           cout << po_konwersji << endl;
           cout << PROTOTYPE << endl;
           cerr << "Blad Charconv\n";
           return -1;
       }
    }
}
