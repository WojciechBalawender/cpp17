#define I_STOP 1000000

#include <tuple>
#include <set>
#include <string>
using namespace std;

struct X {
    string a;
    int b;
    bool operator<(const X & Xx) const
    {
        return std::tie(a,b) < std::tie(Xx.a, Xx.b);
    }
};

inline void test()
{
    std::set<X> zbior;
    X wartosci{"C++", 17};
    auto [iter, wstawiono] = zbior.insert(wartosci);
}

int main()
{
    for(int i = 0; i < I_STOP; i++)
    {
        test();
    }
}
