#include <variant>
#include <cassert>
int main()
{
    std::variant<int, float> w;
    w = 34.7f;
    //int test = std::get<int>(w);
    float test = std::get<float>(w);
    assert(std::holds_alternative<float>(w));
}
