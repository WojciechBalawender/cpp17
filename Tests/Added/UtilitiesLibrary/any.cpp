#include <any>
#include <iostream>

int main()
{
    std::any test = 2.7182818;
    //std::cout << test.type().name() << " : "
    std::any_cast<double>(test);

    test = "ABC";
    //std::cout << test.type().name() << " : "
   //           << std::any_cast<const char*>(test) << '\n';

    try{
        //std::cout <<
        std::any_cast<float>(test);
    }
    catch (const std::bad_any_cast& e) {
        //std::cout << e.what() << '\n';
    }
}
