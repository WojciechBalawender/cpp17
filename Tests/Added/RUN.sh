#!/bin/bash

#set +v
current=$(pwd)

#PREPARE

rm -f list.tmp
find -name '*.cpp' >> list.tmp

#Przygotowanie struktury katalogow:
find . -type d -print0 > ../Logs/struct.tmp
cd ../Bin
xargs -0 mkdir -p < ../Logs/struct.tmp
rm ../Logs/struct.tmp
cd $current

compilers=("${3}")

for compiler in ${compilers[@]} 
do
	echo
	echo -e "\n--- KOMPILACJA z $current\t$(date)" >> ../Logs/results_${compiler}.csv 
	#Kompilacja
	while read -r line; do
		echo -e "\e[96mKompilacja $line...\e[39m"
		
		if [ $compiler == "cl" ] ; then
		    ../stopwatch.${1} "${compiler} /EHsc /std:c++17 $line /Fe../Bin/${line}.${compiler}.${1} >> ../Logs/errors.txt" >> ../Logs/results_${compiler}.csv
		else
			../stopwatch.${1} "${compiler} -S -std=c++17 -fverbose-asm -o ../Bin/${line}.${compiler}.s $line >> ../Logs/errors.txt" >> ../Logs/results_${compiler}.csv
			../stopwatch.${1} "${compiler} -o ../Bin/${line}.${compiler}.${1} ../Bin/${line}.${compiler}.s >> ../Logs/errors.txt" >> ../Logs/results_${compiler}.csv
		
		fi
		done < "${current}/list.tmp"
	echo Zakonczono kompilacje AUTO przy uzyciu ${compiler}

	#Uruhomienie
	echo
	echo -e "\n--- URUCHOMIENIE z $current:\t$(date)" >> ../Logs/results_${compiler}.csv
	while read -r line; do
		if [ -f "../Bin/${line}.${compiler}.${1}" ]; then 
			echo -e "\e[92mUruchomienie ${line}.${compiler}.${1}...\e[39m"
			../stopwatch.${1} "${2} ../Bin/${line}.${compiler}.${1}" >> ../Logs/results_${compiler}.csv
		else
			echo -e "\e[93mPominiecie ${line}.${compiler}.${1} (brak pliku)\e[39m"
			echo -e "Pominiecie ${line}.${compiler}.${1} (brak pliku)\t = ?.???" >> ../Logs/results_${compiler}.csv
		fi
			done < "list.tmp"
done
	rm list.tmp
	find . -name '*.obj' -delete
