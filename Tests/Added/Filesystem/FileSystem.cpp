#define I_STOP 1000
#define FILENAME L"source.dat"
#define FILENAME2 L"source2.dat"
#define FOLDERNAME L"trash"
#include <filesystem>
#include <fstream>

using namespace std::experimental::filesystem;

void new_copy(const wchar_t* source_file, const wchar_t* destination_file)
{
	for (bool again = true; again;)
	try
	{
		create_directory(FOLDERNAME);
		std::ofstream source(source_file, std::ios::binary);
		source << "Testowy plik do skopiowania";
		source.close();
		copy_file(source_file, destination_file);
		remove_all(FOLDERNAME);
		again = false;
	}
	catch (...) {}
}

int main()
{
	for (int i = 0; i < I_STOP; i++)
	{
		new_copy(FILENAME, FOLDERNAME "/" FILENAME2);
	}

}
