auto OldSum(){
    return 0;
}

template<typename first, typename... T>
auto OldSum(first a, T... bcd){
    return a + OldSum(bcd...);
}

int main()
{
   OldSum(98,03,11,14,17);
}
