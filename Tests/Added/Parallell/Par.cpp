#define TEST_SIZE 1'000'000
#define I_STOP 10

#include <algorithm>
#include <random>
#include <vector>
#include <execution>

using namespace std;

int main() {

  random_device rd;
  vector<double> doubles(TEST_SIZE);
  for (auto& d : doubles) {
    d = static_cast<double>(rd());
  }

  for (int i = 0; i < I_STOP; ++i)
  {
    vector<double> liczby(doubles);
    sort(std::execution::par, liczby.begin(), liczby.end());
  }
}
