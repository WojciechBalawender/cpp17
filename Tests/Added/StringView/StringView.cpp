#define I_STOP 20000 //(Wieksza probka = wieksza dokladnosc testu)
#define STRING_SIZE 131047 //(dlugosc badanego stringa)
#define CUT_SIZE 4999 //Ile wyciac
#include <string_view>
#include <iostream>
using namespace std;

string_view prefixRemover (string_view str)
{
	str.remove_prefix(CUT_SIZE);
	return str;
}

int main()
{
    string_view str = string(STRING_SIZE,'#');
    string_view str_after = str.substr(CUT_SIZE);

    for(int i = 0; i < I_STOP; i++)
    {
        auto result = prefixRemover(str);
        if(result != str_after)
           cerr << "NIEPRAWIDLOWA PRACA StringViewTest_A.cpp";
    }
}
