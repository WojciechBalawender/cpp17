#define I_STOP 20000 //(Wieksza probka = wieksza dokladnosc testu)
#define STRING_SIZE 150000 //(dlugosc badanego stringa)
#define CUT_SIZE 4999 //Ile wyciac
#include <string>
#include <iostream>
using namespace std;

string prefixRemover (const string & str)
{
	return str.substr(CUT_SIZE);
}

int main()
{
	string str = string(STRING_SIZE,'#');
    string str_after = str.substr(CUT_SIZE);

    for(int i = 0; i < I_STOP; i++)
    {
        auto result = prefixRemover(str);
        if(result != str_after)
            cerr << "NIEPRAWIDLOWA PRACA StringViewTest_A.cpp";
    }
}
