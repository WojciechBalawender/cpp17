namespace [[deprecated]] test
{
    [[nodiscard]] int fun(int n)
    {
        [[maybe_unused]] int k;
         switch (n) {
            case 1: fun(2);
            [[fallthrough]];
            case 3: return fun(999);
         }
         return 0;
    }
}

int main()
{
    test::fun(343);
}
