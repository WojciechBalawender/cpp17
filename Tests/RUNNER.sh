#!/bin/bash
set +v
tester()
{
	if [ $? -eq 0 ]; then 
		echo -e "Pomyslnie przygotowano \e[1m$1\e[0m" 
	else 
		echo -e "ERROR: \e[41m(brak kompilatora $1 / zmiennej srodowiskowej?)\e[0m"
		if [ $1 == "cl" ] ; then
			echo -e "\e[45mAby naprawic ten problem, uruchom RUNNER.bat\e[49m"
		fi
		exit
	fi
}

TIMEFORMAT=%R
WINDOWS_ENV="Windows_NT Cygwin Msys"

OS=$(uname -o)
echo "Przygotowywanie srodowiska..."
if [[ " ${WINDOWS_ENV[*]} " == *$OS* ]]; then
	echo -e "Srodowisko testowe: \e[1mWindows\e[0m"
	extension="exe"
	g++-7 -o stopwatch.$extension stopwatch.cpp 
	tester stopwatch.cpp
	runparam="start /B /WAIT"
	compilers="g++-7 g++-8 clang++ cl"
else
	echo "Srodowisko testowe: Linux"
	extension="sh"
	g++-7 -o stopwatch.$extension stopwatch.cpp
	tester stopwatch.cpp
	runparam=""
	compilers="g++-7 g++-8 clang++-7"
fi

comp=("${compilers}")
for compiler in ${compilers[@]} 
do
	if [ $compiler == "cl" ] ; then
		${compiler} /EHsc /std:c++17 Utilities/sample.cpp /Fetemp.tmp >/dev/null 2>/dev/null
		rm sample.obj
	else
	${compiler} -o temp.tmp Utilities/sample.cpp
	fi
	tester ${compiler}
	rm temp.tmp
done

echo -e "\e[93m\n-> Przed rozpoczeciem prosze zabic wszystkie zbedne uslugi / procesy systemu.\nW pliku stopwatch.cpp mozna ustawic ilosc powtorzen (stala REPEAT)\e[39m"
read -rsp $'Aby rozpoczac, nacisnij ENTER...\n'

#Przygotowanie katalogw:

rm -rf Bin
rm -rf Logs 
mkdir -p Bin
mkdir -p Logs

#Time:

echo -e "Ostatnie uruchomienie: $(date)\n" > Logs/errors.txt
echo -e "Ostatnie uruchomienie: $(date)\n" > Logs/totalTime.txt

#Dodane
cd Added

{ time ./RUN.sh "$extension" "$runparam" "$compilers" ; } 2>> ../Logs/totalTime.txt

#Usuniete
cd ../Deleted
{ time ./RUN.sh "$extension" "$runparam" "$compilers" ; } 2>> ../Logs/totalTime.txt

#Szczegolne przypadki
cd ../Quaint
{ time ./RUN.sh "$extension" "$runparam" "$compilers" ; } 2>> ../Logs/totalTime.txt

#Wczytanie wynikow & czyszczenie

cd ../

rm -f stopwatch.exe
rm -f stopwatch.out

if [[ " ${WINDOWS_ENV[*]} " == *$OS* ]]; then
	explorer
	cmd /C start Results.ods
else
	libreoffice Results.ods
fi
