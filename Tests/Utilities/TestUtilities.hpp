/*
	Tools that help you measure results.
	Author: Wojciech Balawender
*/

#include <chrono>
#include <iostream>

class Stopwatch //Class to measure time
{

    public:
        Stopwatch() : startTime(std::chrono::system_clock::now()) {/**/}; //Start automatically

        void reset()
        {
            startTime = std::chrono::system_clock::now();
        }

        double result()
        {
            endTime = std::chrono::system_clock::now();
            resultTime = endTime - startTime;
            return resultTime.count();
        }

        private:
            std::chrono::time_point<std::chrono::system_clock> startTime, endTime;
            std::chrono::duration<double> resultTime;
};

#ifdef WIN32
#include <windows.h>
#include <psapi.h>
/*
double MemoryInfo(DWORD processID = GetCurrentProcessId()) //Return private work set for PID (in kilobytes)
{

    HANDLE hProcess;
    PROCESS_MEMORY_COUNTERS_EX pmc;

   // std::cout << "PID process: " << processID << "\n";

    hProcess = OpenProcess(  PROCESS_QUERY_INFORMATION |
                                    PROCESS_VM_READ,
                                    FALSE, processID );
    if (NULL == hProcess)
        return -1;

    if ( GetProcessMemoryInfo( hProcess, (PROCESS_MEMORY_COUNTERS *) &pmc, sizeof(pmc)) )
    {
        double PrivateUsage = pmc.PrivateUsage/1024;
        CloseHandle( hProcess );
        return PrivateUsage;
   //      std::cout << "private work set: " << pmc.PrivateUsage/1024 << " KB \n";
     //    std::cout << "Work set: " << pmc.WorkingSetSize/1024 << " KB \n";
    //     std::cout << "Pagefile: " << pmc.PagefileUsage/1024 << " KB (szczyt: " << pmc.PeakPagefileUsage/1024 << " KB)";
    }
    else return -1;
}
*/
#endif
