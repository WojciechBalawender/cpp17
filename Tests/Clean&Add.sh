#!/bin/bash
rm -rf Bin
cd Logs
find . ! -name 'ResultsAll.ods' -type f -exec rm -f {} +
git add -A
git status