
== Opis:

	Zestaw narzędzi do testowania czasów kompilacji i wykonania programów C++.

---------------------------------
== Uruchomienie w systemie Linux:

1. Pobrać pakiety GCC:
	sudo apt-get install g++-7
	sudo apt-get install g++-8
	sudo apt-get install clang-7 lldb-7 lld-7
	
2. (nadać CHMODy i) uruchomić RUNNER.sh

---------------------------------
== Uruchomienie w systemie Windows:

1. Zainstalowac CYGWIN:
	(https://cygwin.com/setup-x86_64.exe)

2. Zainstalować kompilatory:
	
	(https://sourceforge.net/projects/mingw-w64)
		Version: 7.3.0
		Architecture: x86_64
		Threads: posix
		Exception: seh
	
	MinGW Distro (8.2.0 GCC) (https://nuwen.net/files/mingw/mingw-16.1-without-git.exe)
	
    (Należy zmienić nazwy g++ odpowiednio na g++-7 oraz g++-8)
	
	clang++ (http://releases.llvm.org/7.0.1/LLVM-7.0.1-win64.exe)
	cl (https://visualstudio.microsoft.com/pl/downloads)
	
3. Skonfigurować zmienne środowiskowe:
	
	W pliku RUNNER.bat

4. Uruchomić RUNNER.BAT

---------------------------------
== Dodatkowe informacje:	

	Instrukcja opisuje odtworzenie środowiska zgodnie z pracą magisterską
	"Analiza właściwości funkcjonalnych oraz wydajności standardu C++17",
	nic jednak nie stoi na przeszkodzie, aby dostosować skrypty do pracy z 
	innymi kompilatorami. Należy jedynie pamiętać o odpowiedniej konfiguracji
	zmiennych środowiskowych oraz opcji dla kompilatora (Auto/RUN.sh)

	W pliku stopwatch.cpp można zmienić liczbę powtórzeń (domyślnie: 3)
	
		@@
		Testowane na Linux Mint (64-bit) & Windows 7 (64-bit),
		Intel Celeron CPU G1840, 4 GB RAM, Płyta Asus H81M-K.
		
##################
Update: 09/03/2019 - Kosmetyka
Update: 06/01/2019 - Usunięcie binarek instalacyjnych (kopia lokalna)
Update: 05/01/2019
